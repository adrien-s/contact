#ifndef __CHAT_H__
#define __CHAT_H__

struct chatContact {
	char * name;				// Name of the contact
	char * color;				// ANSI escape color (ex 1;31)
	char * chatPrefix;			// Prefix (eg +, @.. for irc chats)
};

struct chatEntry {
	struct chatContact * contact;		// Contact
	long int timestamp;			// Timestamp of entry
	char * text;				// Text of the entry
};

struct chat {
	struct chatEntry ** entries;		// Entries of this chat
	struct chatContact ** contacts;		// Contacts linked to this chat
	struct chatContact * lastActive;	// Last active contact in the chat
	unsigned long int numEntries;
	unsigned long int numContacts;		// Number of total contacts in this chat
	char * service;				// eg chat.freenode.net, facebook.com, etc
	char * chatName;			// eg for irc: #chanName, fb: contact/chat room name..
};

struct chat * chat_init(char * service, char * chatName);
void chat_free(struct chat * c);

void chat_add_entry(struct chat * c, struct chatEntry * e);
void chat_add_manual_entry(struct chat * c, struct chatContact * co, long int timestamp, char * text);
void chat_add_contact(struct chat * c, struct chatContact * co);

#endif
