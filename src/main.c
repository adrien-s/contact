#include <stdio.h>
#include <stdlib.h>

#include "chat/chat.h"

int main(int argc, char ** argv)
{
	struct chat a;
	a.chatName = "#bronycub";
	a.service = "chat.freenode.net:6667";
	a.numEntries = 0;

	printf("\033[2J\033[1;2H\033[1m%s\033[0m\033[2;3H%s\n", a.service, a.chatName);
	return 0;
}
