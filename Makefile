include config.mk
TARGET=contact
TARGETS=src/*/*.o src/*.o
SUBTARGETS=chat main

# Target directories
DESTDIR:=
PREFIX:=$(DESTDIR)/usr
BINDIR:=$(PREFIX)/bin



.PHONY: clean mrproper build chat install

# Main rule
build: $(SUBTARGETS)
	@[ -d bin ] || mkdir bin
	@echo "[1;34m[linking][0;34m $(TARGET)[0m"
	@$(CC) $(CFLAGS) $(LDFLAGS) $(TARGETS) -o bin/$(TARGET)



# Building rules
%.o: %.c
	@echo "[1;32m[build/main][0;32m $@[0m"
	@$(CC) $(CFLAGS) -c $< -o $@

main: src/main.o

chat:
	@$(MAKE) -C src/chat build



# Admin rules
mrproper: clean
	rm -f bin/$(TARGET)

clean:
	rm -f src/*.o
	@$(MAKE) -C src/chat clean

install: bin/$(TARGET)
	install -Dm755 bin/$(TARGET) $(BINDIR)/$(TARGET)

uninstall:
	rm $(BINDIR)/$(TARGET)
